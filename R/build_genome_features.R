#' @title  build a convenient genomic features object.
#' @description build a convenient \code{\link{genome_features-class}} object needed for find overlaps between
#' selected features and genomic coordinates in a single step.
#' @importFrom AnnotationDbi select keys as.data.frame metadata
#' @importFrom GenomicFeatures promoters transcripts fiveUTRsByTranscript threeUTRsByTranscript exonsBy intronsByTranscript cdsBy
#' @import S4Vectors
#' @importFrom GenomicRanges flank
#' @importFrom rtracklayer import
#' @importFrom  BiocGenerics as.data.frame
#' @importFrom data.table data.table := .N like as.data.table
#' @param txdb a txdb database built with \pkg{GenomicFeatures} (see example).
#' @param features a \code{\link[base]{vector}} of features \code{\link[base]{character}} with by
#' default all the availables features (("promoter","UTR5","UTR3","exon1","exons","intron1","introns","cds", and "downstream").
#' @param parameters a \code{\link[base]{list}} of parameters to use in order to define promoter and
#' downstream ranges distance to TSS (Transcript Start Site) or TES (Transcript End Site).
#' \itemize{
#'  \item{promoter_ranges: a \code{\link[base]{list}} with upstream and dowstream  elements distance (in base) to TSS}.
#'  \item{downstream_range: the dowstream distance (in base) to the TES}.
#' }
#' @details This method build a convenient \code{\link{genomic_features-class}} object, in order to be used for finding overlaps between
#' all selected features and genomic coordinates with the \code{\link{genome_features_overlaps}} method.
#' @return a \code{\link{genome_features-class}} object.
#' @include genome_features.R
#' @examples
#  # import the chicken genome GFF from NCBI ftp (galGal5 in this case)
#' download.file(
#'     "ftp://ftp.ncbi.nlm.nih.gov/genomes/Gallus_gallus/GFF/ref_Gallus_gallus-5.0_top_level.gff3.gz",
#'     quiet=TRUE,
#'     destfile = "galGal5_gff.gz"
#' )
#'
#' # uncompress the file
#' R.gunzip("galGal5_gff.gz")
#'
#' # build the database
#' toplevel=makeTxDbFromGFF(
#'     file="galGal5_gff",
#'     format="gff",
#'     organism="Gallus gallus",
#'     taxonomyId=9031,
#'     dbxrefTag="ID"
#' )
#'
#' # build genome_features object from txdb
#' features<-GenomeFeatures::build_genome_features(
#'     toplevel,
#'     features=c("promoter","UTR5","UTR3","exons","introns","cds","downstream"),
#'     parameters = list(
#'         promoter_ranges=list(
#'             upstream=3000,
#'             downstream=500
#'         ),
#'         downstream_range=1000
#'     )
#' )
#' @name build_genome_features
#' @rdname build_genome_features-methods
#' @exportMethod build_genome_features
setGeneric(
    name="build_genome_features",
    def=function(
        txdb,features=c("promoter","UTR5","UTR3","exon1","exons","intron1","introns","cds","downstream"),
        parameters=list(promoter_ranges=list(upstream=3000,downstream=500),downstream_range=1000)){
            standardGeneric("build_genome_features")
    }
)

#' @rdname build_genome_features-methods
#' @aliases build_genome_features
setMethod(
    "build_genome_features",
    signature(
        txdb="TxDb"
    ),
    definition=function(txdb,features,parameters) {

        ## txdb ids crossref

        # build ids cross reference
        crossref<-data.table(
            select(
                txdb,
                columns=c("TXID","TXNAME"),
                keytype="GENEID",
                keys=keys(txdb)
            )
        )

        # convert TXID to character
        crossref[,"TXID":=as.character(TXID)]

        ## promoters

        promoters=NULL;DT=NULL

        if("promoter"%in%features){

            # get promoters
            promoters <-promoters(
                txdb,
                upstream=parameters$promoter_ranges$upstream,
                downstream=parameters$promoter_ranges$downstream
            )

            # convert to table
            DT<-data.table(
                as.data.frame(
                    promoters,
                    row.names = NULL
                )
            )[,c("tx_id","tx_name"),with=FALSE]

            # convert to character
            DT[,"tx_id":=as.character(DT$tx_id)]

            # merge with crossref
            DT<-merge(
                DT,
                crossref,
                by.x="tx_id",
                by.y="TXID",
                sort=FALSE,
              all.x=TRUE
            )

            # rename
            names(DT)[1:2]<-c("group","feature")

            # format feature
            DT[,"tx_name":=feature]

            # format feature
            DT[,
                `:=`(
                    feature=paste(
                        DT$GENEID,
                        DT$feature,
                        "promoter",
                        sep=":"
                    ),
                    GENEID=NULL,
                    TXNAME=NULL
                )
            ]

            # assign metadata
            mcols(promoters)<-DT
        }

        ## downstream

        downstream=NULL;DT=NULL

        if("downstream"%in%features){

            # get downstream
            downstream<-transcripts(txdb)
            downstream<-flank(
                downstream,
                parameters$downstream_range,
                start=FALSE
            )

            # convert to table
            DT<-data.table(
                as.data.frame(
                    downstream,
                    row.names = NULL
                )
            )[,c("tx_id","tx_name"),with=FALSE]

            # convert to character
            DT[,"tx_id":=as.character(DT$tx_id)]

            # merge with crossref
            DT<-merge(
                DT,
                crossref,
                by.x="tx_id",
                by.y="TXID",
                sort=FALSE,
                all.x=TRUE
            )

            # rename
            names(DT)[1:2]<-c("group","feature")

            # format feature
            DT[,"tx_name":=feature]

            # format feature
            DT[,
                `:=`(
                    feature=paste(
                        DT$GENEID,
                        DT$feature,
                        "downstream",
                        sep=":"
                    ),
                    GENEID=NULL,
                    TXNAME=NULL
                )
            ]

            # assign metadata
            mcols(downstream)<-DT
        }

        ## 5' UTR

        UTR5=NULL;DT=NULL

        if("UTR5"%in%features){

            # get 5' UTR in GRangesList
            UTR5<-fiveUTRsByTranscript(txdb,use.names=FALSE)

            # convert to table
            DT<-data.table(
                as.data.frame(
                    UTR5,
                    row.names = NULL
                )
            )[,c("group_name","exon_rank"),with=FALSE]

            # convert to GRanges
            UTR5<-unlist(UTR5)

            # merge with crossref
            DT<-merge(
                DT,
                crossref,
                by.x="group_name",
                by.y="TXID",
                sort=FALSE,
                all.x=TRUE
            )

            # format feature
            DT[,
                `:=`(
                    feature=paste(
                        DT$GENEID,
                        ":",
                        DT$TXNAME,
                        ":5'UTR(exon",
                        DT$exon_rank,
                        ")",
                        sep=""
                    ),
                    exon_rank=NULL,
                    GENEID=NULL,
                    tx_name=DT$TXNAME,
                    TXNAME=NULL,
                    group_name=NULL
                )
            ]

            # assign metadata
            mcols(UTR5)<-DT

        }

        ## 3' UTR

        UTR3=NULL;DT=NULL

        if("UTR3"%in%features){

            # get 3' UTR
            UTR3<-threeUTRsByTranscript(txdb,use.names=FALSE)

            # convert to table
            DT<-data.table(
                as.data.frame(
                    UTR3,
                    row.names = NULL
                )
            )[,c("group_name","exon_rank"),with=FALSE]

            # convert to GRanges
            UTR3<-unlist(UTR3)

            # merge with crossref
            DT<-merge(
                DT,
                crossref,
                by.x="group_name",
                by.y="TXID",
                sort=FALSE,
                all.x=TRUE
            )

            # format feature
            DT[,
                `:=`(
                    feature=paste(
                        DT$GENEID,
                        ":",
                        DT$TXNAME,
                        ":3'UTR(exon",
                        DT$exon_rank,
                        ")",
                        sep=""
                    ),
                    exon_rank=NULL,
                    GENEID=NULL,
                    tx_name= DT$TXNAME,
                    TXNAME=NULL,
                    group_name=NULL
                )
            ]

            # assign metadata
            mcols(UTR3)<-DT
        }

        ## Exons

        exons=NULL;exon1=NULL;DT=NULL

        if("exons"%in%features  | "exon1"%in%features){

            # get exons
            exons<-exonsBy(txdb,use.names=FALSE)

            # convert to table
            DT<-data.table(
                as.data.frame(
                    exons,
                    row.names = NULL
                )
            )[,c("group_name","exon_rank"),with=FALSE]

            # convert to GRanges
            exons<-unlist(exons)

            # count exons by transcripts
            exons_nb<-DT[!is.na(group_name),.N,by="group_name"]

            # merge count to exons_DT
            DT<-merge(
                DT,
                exons_nb,
                by="group_name",
                all.x=TRUE,
                sort=FALSE
            )

            # merge with crossref
            DT<-merge(
                DT,
                crossref,
                by.x="group_name",
                by.y="TXID",
                sort=FALSE,
                all.x=TRUE
            )

            # format feature
            DT[,
                `:=`(
                    feature=paste(
                        DT$GENEID,
                        ":",
                        DT$TXNAME,
                        ":exon",
                        DT$exon_rank,
                        "/",
                        DT$N,
                        sep=""
                    ),
                    exon_rank=NULL,
                    GENEID=NULL,
                    tx_name= DT$TXNAME,
                    TXNAME=NULL,
                    N=NULL,
                    group_name=NULL
                )
            ]

            # assign metadata
            mcols(exons)<-DT

            if("exon1"%in%features){

              # locate intron 1
              pos<-grepl("exon1/",DT[,feature])

              # subset intron1
              DT<-DT[pos]

              # get intron1 GRanges
              exon1<-exons[pos]

              # assign metadata
              mcols(exon1)<-DT
            }
        }

        ## Introns

        introns=NULL;intron1=NULL;DT=NULL

        if("introns"%in%features | "intron1"%in%features){

            # get introns
            introns<-intronsByTranscript(txdb,use.names=FALSE)

            # convert to table
            DT<-data.table(
                as.data.frame(
                    introns,
                    row.names = NULL
                )
            )[,c("group_name","strand"),with=FALSE]

            # convert to GRanges
            introns<-unlist(introns)

            # count introns by transcripts
            introns_nb<-DT[!is.na(group_name),.N,by="group_name"]

            # merge count to introns_DT
            DT<-merge(
                DT,
                introns_nb,
                by="group_name",
                all.x=TRUE,
                sort=FALSE
            )

            # add intron_rank
            DT[,"intron_rank":=1:length(N),by="group_name"]

            # reverse intron_rank for reverse strand
            DT["-","intron_rank":=rev(intron_rank),by="group_name",on="strand"]

            # merge with crossref
            DT<-merge(
                DT,
                crossref,
                by.x="group_name",
                by.y="TXID",
                sort=FALSE,
                all.x=TRUE
            )

            # format feature
            DT[,
                `:=`(
                    feature=paste(
                        DT$GENEID,
                        ":",
                        DT$TXNAME,
                        ":intron",
                        DT$intron_rank,
                        "/",
                        DT$N,
                        sep=""
                    ),
                    intron_rank=NULL,
                    strand=NULL,
                    GENEID=NULL,
                    tx_name=DT$TXNAME,
                    TXNAME=NULL,
                    N=NULL,
                    group_name=NULL
                )
            ]

            # assign metadata
            mcols(introns)<-DT

            if("intron1"%in%features){

                # locate intron 1
                pos<-grepl("intron1/",DT[,feature])

                # subset intron1
                DT<-DT[pos]

                # get intron1 GRanges
                intron1<-introns[pos]

                # assign metadata
                mcols(intron1)<-DT
            }
        }

        ## CDS

        cds=NULL;DT=NULL

        if("cds"%in%features){

            # get exons
            cds<-cdsBy(txdb,use.names=FALSE)

            # convert to table
            DT<-data.table(
                as.data.frame(
                    cds,
                    row.names = NULL
                )
            )[,c("group_name","exon_rank"),with=FALSE]

            # convert to GRanges
            cds<-unlist(cds)

            # count exons by transcripts
            cds_nb<-DT[!is.na(group_name),.N,by="group_name"]

            # merge count to exons_DT
            DT<-merge(
                DT,
                cds_nb,
                by="group_name",
                all.x=TRUE,
                sort=FALSE
            )

            # merge with crossref
            DT<-merge(
                DT,
                crossref,
                by.x="group_name",
                by.y="TXID",
                sort=FALSE,
                all.x=TRUE
            )

            # format feature
            DT[,
                `:=`(
                    feature=paste(
                        DT$GENEID,
                        ":",
                        DT$TXNAME,
                        ":exon",
                        DT$exon_rank,
                        "/",
                        DT$N,
                        sep=""
                    ),
                    exon_rank=NULL,
                    GENEID=NULL,
                    tx_name=DT$TXNAME,
                    TXNAME=NULL,
                    N=NULL,
                    group_name=NULL
                )
            ]

            # assign metadata
            mcols(cds)<-DT
        }

        # load annotation complement

        # check file name
        file=metadata(txdb)$value[3]

        if(!is.na(file)){

            # download
            if(length(grep("ftp.+\\.gz$",file))>0){

                # output name
                output=sub("^.+/","",file)

                # import the GFF
                download.file(
                    file,
                    quiet=TRUE,
                    destfile=output
                )

                # uncompress
                R.gunzip(output)

                # file to download
                file=output
            }

            # read the gene file as GRanges
            gene_table<-rtracklayer::import(file)

            # convert to data.table
            gene_table<-data.table(
                as.data.frame(
                    gene_table,
                    row.names = NULL
                )
            )[like(type,"RNA|transcript"),]

            if(length(grep("gtf",file))>0){

                # select columns from gtf
                gene_table<-gene_table[,
                    c("seqnames","start","end","strand","type","gene_name","transcript_id","transcript_biotype","type","gene_id"),
                    with=FALSE
                ]

            }else{

                # select columns from gff
                gene_table<-gene_table[,
                    c("seqnames","start","end","strand","type","Dbxref","gene","transcript_id"),
                    with=FALSE
                ]

                # extract gene_id
                gene_table[,
                    gene_id:=sub("^.+:","",Dbxref[[1]][1]),
                    by=1:nrow(gene_table)
                ][,"Dbxref":=NULL]

                # reneme gene to gene_name
                names(gene_table)[6]<-"gene_name"

            }

  }else{

            # else NA
            gene_table=data.table(gene_table=NA)
        }

        ## create genome_features

        # create genome_features
        new(
            "genome_features",
            metadata=list(data.table(metadata(txdb))),
            promoter=list(GRanges=promoters),
            UTR5=list(GRanges=UTR5),
            UTR3=list(GRanges=UTR3),
            exons=list(GRanges=exons),
            exon1=list(GRanges=exon1),
            introns=list(GRanges=introns),
            intron1=list(GRanges=intron1),
            cds=list(GRanges=cds),
            downstream=list(GRanges=downstream),
            gene_table=gene_table
        )
    }
)
