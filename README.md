<img src="./inst/extdata/univ.png" align="right"/>
<img src="./inst/extdata/boa.png" align="right"/>
<img src="./inst/extdata/inra.png" align="right"/>

# GenomeFeatures R package

The aim of this package is to provide a full genomic features annotation from genomic coordinates, without use an annotation priority.

## Installation

```r
## using install_gitlab
remotes::install_gitlab(
    "aurelien.brionne/GenomeFeatures",
    host = "forgemia.inra.fr",
    build_opts = c("--no-resave-data", "--no-manual")
)

## alternative (from local directory)
    # clone package (from prompt)
    git clone https://forgemia.inra.fr/aurelien.brionne/GenomeFeatures.git

    # build package (from R console) 
    devtools::build("GenomeFeatures")

    # install package (from R console)
    install.packages("GenomeFeatures_1.1.tar.gz", repos = NULL, type = "source")
```

## Quick overview

1. load peaks file: `GenomeFeatures::readPeakFile`.

2. Build a convenient object for find overlaps beetween all selected features and genomic coordinates targets: `GenomeFeatures::build_genome_features`.

3. Find overlaps beetween genomic coordinates and features: `GenomeFeatures::genome_features_overlaps`.

4. Features plot: `GenomeFeatures::Plot`.

![](./inst/extdata/barplot.png)

![](./inst/extdata/lineplot.png)

5. Get annotation table: `GenomeFeatures::Table`.

![](./inst/extdata/table.png)

6. Features ovelaps.

![](./inst/extdata/data/output/upset_A.png)
