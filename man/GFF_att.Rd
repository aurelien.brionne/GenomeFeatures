% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/GFF_att.R
\name{GFF_att}
\alias{GFF_att}
\title{Extract attributes from gff.}
\usage{
GFF_att(..., att.list = c("GeneID", "Name", "gene"))
}
\value{
a \code{\link[data.table]{data.table}}.
}
\description{
Extract attributes from gff file  \code{\link[data.table]{data.table}}.
}
\details{
This internal function is use by \code{\link{build_genome_features}} in order to extract attributes from gff
and return a  \code{\link[data.table]{data.table}}.
}
\keyword{internal}
